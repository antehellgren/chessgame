package se.experis.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.backend.models.PlayerStats;

public interface PlayerStatsRepository extends JpaRepository<PlayerStats,Long> {
}
