package se.experis.backend.repositories;

import se.experis.backend.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {
    Boolean existsByUid(String uid);
    Player getByUid(String uid);
}
