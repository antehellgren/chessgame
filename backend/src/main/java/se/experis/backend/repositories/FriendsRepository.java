package se.experis.backend.repositories;

import se.experis.backend.models.Friends;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendsRepository extends JpaRepository<Friends,String> {
}
