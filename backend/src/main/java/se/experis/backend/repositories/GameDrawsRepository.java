package se.experis.backend.repositories;

import se.experis.backend.models.GameDraws;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameDrawsRepository extends JpaRepository<GameDraws,Long> {
    List<GameDraws> getByGameId(Long gameId);
}
