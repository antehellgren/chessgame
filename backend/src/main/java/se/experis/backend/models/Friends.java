package se.experis.backend.models;


import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;

@Entity
public class Friends {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String friendId;

    private String username;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @JsonGetter("player")
    public String players() {
        if (player != null) {
            return "/api/v1/players/" + player.getUid();
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }
}
