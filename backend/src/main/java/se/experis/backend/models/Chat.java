package se.experis.backend.models;


import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "chat")
    List<Message> message;

    @JsonGetter("message")
    public List<String> message() {
        if (message != null) {
            return message.stream()
                    .map(message -> {
                        return "/api/v1/message/" + message.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }
    @ManyToMany
    @JoinTable(
            name = "player_chat",
            joinColumns = {@JoinColumn(name = "chat_id")},
            inverseJoinColumns = {@JoinColumn(name = "player_id")}
    )
    public List<Player> player = new ArrayList<>();

    @JsonGetter("player")
    public List<String> player() {
        if (player != null) {
            return player.stream()
                    .map(player -> {
                        return "/api/v1/players/" + player.getUid();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Player> getPlayer() {
        return player;
    }

    public void setPlayer(List<Player> player) {
        this.player = player;
    }

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }
}
