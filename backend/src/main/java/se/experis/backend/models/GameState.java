package se.experis.backend.models;

public enum GameState {
    REGISTRATION,
    IN_PROGRESS,
    COMPLETE
}
