package se.experis.backend.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Player {

    @Id
    private String uid;

    @Column
    private String username;

    @Column
    private String email;

    @OneToOne
    @JoinColumn(name = "player_stats_id")
    private PlayerStats playerStats;

    @JsonGetter("playerStats")
    public String playerStats() {
        if (playerStats != null) {
            return "/api/v1/stats/" + playerStats.getId();
        }
        return null;
    }

    @ManyToMany
    @JoinTable(
            name = "player_chat",
            joinColumns = {@JoinColumn(name = "player_id")},
            inverseJoinColumns = {@JoinColumn(name = "chat_id")}
    )
    public List<Chat> chat = new ArrayList<>();

    @JsonGetter("chat")
    public List<String> chat() {
        if (chat != null) {
            return chat.stream()
                    .map(chat -> {
                        return "/api/v1/chat/" + chat.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    @OneToMany(mappedBy = "player")
    List<Friends> friends;

    @JsonGetter("friends")
    public List<String> friends() {
        if (friends != null) {
            return friends.stream()
                    .map(friends -> {
                        return "/api/v1/friends/" + friends.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

   @ManyToMany
    @JoinColumn(name = "gameId")
    private List<Game> game;


    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PlayerStats getPlayerStats() {
        return playerStats;
    }

    public void setPlayerStats(PlayerStats playerStats) {
        this.playerStats = playerStats;
    }

   public List<Friends> getFriends() {
        return friends;
    }

    public void setFriends(List<Friends> friends) {
        this.friends = friends;
    }

   public List<Chat> getChat() {
        return chat;
    }

    public void setChat(List<Chat> chat) {
        this.chat = chat;
    }

    public List<Game> getGame() {
        return game;
    }

    public void setGame(List<Game> game) {
        this.game = game;
    }
}
