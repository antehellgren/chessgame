package se.experis.backend.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Game {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GameState gameState;

    @ManyToMany(mappedBy = "game", cascade=CascadeType.ALL)
    private List<Player> players = new ArrayList<>();

    @JsonGetter("players")
    public List<String> players() {
        return players.stream()
                .map(player -> {
                    return "/api/v1/game/" + this.getId() + "/player/" + player.getUid();
                }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "game", cascade=CascadeType.ALL)
    private List<GameDraws> gameDraws = new ArrayList<>();

    @JsonGetter("gameDraws")
    public List<String> gameDraws() {
        return gameDraws.stream()
                .map(draw -> {
                    return "/api/v1/game/" + this.getId() + "/draws/" + draw.getId();
                }).collect(Collectors.toList());
    }



    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<GameDraws> getGameDraws() {
        return gameDraws;
    }

    public void setGameDraws(List<GameDraws> gameDraws) {
        this.gameDraws = gameDraws;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
