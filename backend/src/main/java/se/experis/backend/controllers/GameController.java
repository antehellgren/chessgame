package se.experis.backend.controllers;


import se.experis.backend.models.Game;
import se.experis.backend.models.GameState;
import se.experis.backend.models.Player;
import se.experis.backend.repositories.GameRepository;
import se.experis.backend.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/game")
public class GameController {

    @Autowired
    private GameRepository gameRepository;
    private HttpStatus httpStatus;

    @Autowired
    private PlayerRepository playerRepository;


    @GetMapping("/test")
    public String testDepoy(){
        return "TJENARE";
    }

    @GetMapping
    public ResponseEntity<List<Game>> getAllGames() {
        List<Game> games = gameRepository.findAll();
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(games, httpStatus);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<Game> getSpecificGame(@PathVariable Long gameId) {
        Game returnGame = new Game();

        if (!gameRepository.existsById(gameId)) {
            httpStatus = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnGame, httpStatus);
        }

        httpStatus = HttpStatus.OK;
        returnGame = gameRepository.findById(gameId).get();
        return new ResponseEntity<>(returnGame, httpStatus);
    }

    @PostMapping
    public ResponseEntity<Game> addGame(@RequestBody Game game) {
        Game newGame = gameRepository.save(game);
        httpStatus = HttpStatus.CREATED;
        return new ResponseEntity<>(newGame, httpStatus);
    }

    @PatchMapping("/{gameId}")
    public ResponseEntity<Game> patchGame(@RequestBody Game updatedGame, @PathVariable Long gameId) {
        Game returnGame = new Game();

        if (gameId != updatedGame.getId()) {
            httpStatus = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnGame, httpStatus);
        }

        Game game = gameRepository.findById(gameId).get();
        game.setGameState(updatedGame.getGameState());

        returnGame = gameRepository.save(game);
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(returnGame, httpStatus);
    }


    @PostMapping("/{gameId}/join")
    public ResponseEntity<Game> joinGame(@PathVariable Long gameId, @RequestBody Player player) {
        Game returnGame = new Game();

        if (!gameRepository.existsById(gameId) || gameRepository.getById(gameId).getPlayers().size() > 1) {
            httpStatus = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnGame, httpStatus);
        }

        Game game = gameRepository.getById(gameId);

        List<Player> players = game.getPlayers();
        players.add(playerRepository.getByUid(player.getUid()));
        game.setPlayers(players);

        Player getPlayer = playerRepository.getByUid(player.getUid());
        List<Game> playerGames = getPlayer.getGame();
        playerGames.add(game);
        getPlayer.setGame(playerGames);

        playerRepository.getByUid(player.getUid()).setGame(playerGames);

        returnGame = gameRepository.save(game);
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(returnGame, httpStatus);
    }


    @PutMapping("/{gameId}/start")
    public ResponseEntity<Game> startGame(@PathVariable Long gameId) {
        Game updatedGame = new Game();

        if (!gameRepository.existsById(gameId)) {
            httpStatus = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(updatedGame, httpStatus);
        }

        updatedGame = gameRepository.findById(gameId).get();
        updatedGame.setGameState(GameState.IN_PROGRESS);
        updatedGame = gameRepository.save(updatedGame);
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(updatedGame, httpStatus);
    }

    @PutMapping("/{gameId}/end")
    public ResponseEntity<Game> endGame(@PathVariable Long gameId) {
        Game updatedGame = new Game();

        if (!gameRepository.existsById(gameId)) {
            httpStatus = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(updatedGame, httpStatus);
        }

        updatedGame = gameRepository.findById(gameId).get();
        updatedGame.setGameState(GameState.COMPLETE);
        updatedGame = gameRepository.save(updatedGame);
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(updatedGame, httpStatus);
    }

    @DeleteMapping("/{gameId}")
    public ResponseEntity<Game> deleteGame(@PathVariable Long gameId) {

        if (!gameRepository.existsById(gameId)) {
            httpStatus = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(httpStatus);
        }

        httpStatus = HttpStatus.OK;
        gameRepository.deleteById(gameId);
        return new ResponseEntity<>(httpStatus);
    }
}

