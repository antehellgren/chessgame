package se.experis.backend.controllers;


import se.experis.backend.models.Chat;
import se.experis.backend.repositories.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/chat")
public class ChatController {

    @Autowired
    ChatRepository chatRepository;
    HttpStatus httpStatus;

    @GetMapping("/{id}")
    public ResponseEntity<Chat> getChatById(@PathVariable Long id) {
        Chat returnChat = new Chat();
        HttpStatus status;

        if (chatRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnChat = chatRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnChat, status);
    }
    @PostMapping
    public ResponseEntity<Chat> addChat(@RequestBody Chat chat) {
        httpStatus = HttpStatus.FORBIDDEN;
        try {
            chat = chatRepository.save(chat);
            httpStatus = HttpStatus.CREATED;
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(chat, httpStatus);
    }
}