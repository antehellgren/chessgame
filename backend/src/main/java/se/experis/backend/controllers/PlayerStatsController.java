package se.experis.backend.controllers;


import se.experis.backend.models.PlayerStats;
import se.experis.backend.repositories.PlayerStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/stats")
public class PlayerStatsController {
    @Autowired
    private PlayerStatsRepository playerStatsRepository;
    private HttpStatus httpStatus;

    @GetMapping("list")
    public Iterable<PlayerStats> getPlayerStats() {
        return this.playerStatsRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PlayerStats> getPlayerStatsById(@PathVariable Long id) {
        PlayerStats returnPlayerStats = new PlayerStats();
        HttpStatus status;

        if (playerStatsRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnPlayerStats = playerStatsRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnPlayerStats, status);
    }

    @PostMapping
    public ResponseEntity<PlayerStats> addPlayerStats(@RequestBody PlayerStats playerStats) {
        httpStatus = HttpStatus.FORBIDDEN;
        try {
            playerStats = playerStatsRepository.save(playerStats);
            httpStatus = HttpStatus.CREATED;
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(playerStats, httpStatus);
    }


    @PatchMapping("{id}")
    public ResponseEntity<PlayerStats> updatePlayerStats(@PathVariable Long id, @RequestBody PlayerStats changedPlayerStats) {
        PlayerStats playerStats = playerStatsRepository.findById(id).get();
        httpStatus = HttpStatus.OK;
        boolean needsUpdate = false;
        if (needsUpdate) {
            playerStatsRepository.save(playerStats);
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(playerStats, httpStatus);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<PlayerStats> deletePlayerStats(@PathVariable Long id) {
        if (playerStatsRepository.existsById(id)) {
            playerStatsRepository.deleteById(id);
            httpStatus = HttpStatus.OK;
        }
        else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }
}

