package se.experis.backend.controllers;


import se.experis.backend.models.Chat;
import se.experis.backend.models.Friends;
import se.experis.backend.models.Player;
import se.experis.backend.repositories.ChatRepository;
import se.experis.backend.repositories.FriendsRepository;
import se.experis.backend.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/friends")
public class FriendsController {

    @Autowired
    private FriendsRepository friendsRepository;
    HttpStatus httpStatus;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ChatRepository chatRepository;

    @GetMapping("list")
    public Iterable<Friends> getFriends() {
        return this.friendsRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Friends> getFriendsById(@PathVariable String id) {
        Friends returnFriend = new Friends();
        HttpStatus status;

        if (friendsRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnFriend = friendsRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFriend, status);
    }


    @PostMapping({"/{playerId}"})
    public ResponseEntity<Friends> addFriend(@RequestBody Friends friends, @PathVariable String playerId) {
        try {
            List<Player> playerList = this.playerRepository.findAll();
            Friends returnFriend = new Friends();
            int counter = 0;
            Chat chat = new Chat();
            for (int i = 0; i < playerList.size(); i++) {
                if (friends.getUsername().equals(playerList.get(i).getUsername())) {
                    friends.setFriendId(playerList.get(i).getUid());
                    friends.setPlayer(playerRepository.getByUid(playerId));
                    returnFriend.setFriendId(playerRepository.getByUid(playerId).getUid());
                    returnFriend.setPlayer(playerList.get(i));
                    returnFriend.setUsername(playerRepository.getByUid(playerId).getUsername());
                    List<Player> playersInChat = new ArrayList<>();
                    playersInChat.add(playerRepository.getByUid(playerList.get(i).getUid()));
                    playersInChat.add(playerRepository.getByUid(playerId));
                    chat.setPlayer(playersInChat);
                    httpStatus = HttpStatus.ALREADY_REPORTED;

                }
            }
            for (int j = 0; j < playerRepository.getByUid(playerId).getFriends().size(); j++) {
                if (!playerRepository.getByUid(playerId).getFriends().get(j).getUsername().equals(friends.getUsername())) {
                    counter++;
                }
            }
            if (counter >= playerRepository.getByUid(playerId).getFriends().size() && !(playerRepository.getByUid(playerId).getUsername().equals(friends.getUsername()))) {
                chatRepository.save(chat);
                friendsRepository.save(returnFriend);
                friends = friendsRepository.save(friends);
                httpStatus = HttpStatus.CREATED;
            }

        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(friends, httpStatus);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Friends> deleteFriend(@PathVariable String id) {
        if (friendsRepository.existsById(id)) {
            friendsRepository.deleteById(id);
            httpStatus = HttpStatus.OK;
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }
}