package se.experis.backend.controllers;


import se.experis.backend.models.GameDraws;
import se.experis.backend.repositories.GameDrawsRepository;
import se.experis.backend.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/game/{gameId}/draws")
public class GameDrawsController {

    @Autowired
    private GameDrawsRepository gameDrawsRepository;

    @Autowired
    private GameRepository gameRepository;

    private HttpStatus httpStatus;

    @GetMapping
    public ResponseEntity<List<GameDraws>> getAllDraws(@PathVariable Long gameId) {
        List<GameDraws> draws = gameDrawsRepository.getByGameId(gameId);
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(draws, httpStatus);
    }

    @GetMapping("/{drawId}")
    public ResponseEntity<GameDraws> getDrawById(@PathVariable Long drawId, @PathVariable Long gameId) {
        GameDraws returnGameDraw = new GameDraws();

        if (!gameRepository.existsById(gameId) || !gameDrawsRepository.existsById(drawId)) {
            httpStatus = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnGameDraw, httpStatus);
        }

        httpStatus = HttpStatus.OK;
        returnGameDraw = gameDrawsRepository.findById(drawId).get();
        return new ResponseEntity<>(returnGameDraw, httpStatus);
    }

    @PostMapping
    public ResponseEntity<GameDraws> addDraw(@PathVariable Long gameId, @RequestBody GameDraws gameDraws){
        GameDraws returnDraw = new GameDraws();

        if(!gameRepository.existsById(gameId)){
            httpStatus = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnDraw, httpStatus);
        }

        returnDraw = gameDrawsRepository.save(gameDraws);
        httpStatus = HttpStatus.CREATED;
        return new ResponseEntity<>(returnDraw, httpStatus);
    }

}
