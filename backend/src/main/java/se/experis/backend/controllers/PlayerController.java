package se.experis.backend.controllers;


import se.experis.backend.models.Player;
import se.experis.backend.models.PlayerStats;
import se.experis.backend.repositories.PlayerRepository;
import se.experis.backend.repositories.PlayerStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/players")
public class PlayerController {

    @Autowired
    private PlayerRepository playerRepository;
    private HttpStatus httpStatus;

    @Autowired
    private PlayerStatsRepository playerStatsRepository;

    @GetMapping("list")
    public Iterable<Player> getPlayer() {
        return this.playerRepository.findAll();
    }

    @GetMapping("/{uid}")
    public ResponseEntity<Player> getPlayerById(@PathVariable String uid) {
        Player returnPlayer = new Player();
        HttpStatus status;
        System.out.println(playerRepository.existsByUid(uid));
        if (playerRepository.existsByUid(uid)) {
            status = HttpStatus.OK;
            returnPlayer = playerRepository.getByUid(uid);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnPlayer, status);
    }

    @PostMapping
    public ResponseEntity<Player> addPlayer(@RequestBody Player player) {
        try {
            PlayerStats playerStats = new PlayerStats();
            playerStats.setWins(0);
            playerStats.setDraws(0);
            playerStats.setLosses(0);
            playerStats.setRating(1000);
            playerStatsRepository.save(playerStats);
            player.setPlayerStats(playerStats);
            player = playerRepository.save(player);
            httpStatus = HttpStatus.CREATED;
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(player, httpStatus);
    }


/*    @PatchMapping("{id}")
    public ResponseEntity<Player> updatePlayer(@PathVariable Long id, @RequestBody Player changedPlayer) {
        Player player = playerRepository.findById(id).get();
        httpStatus = HttpStatus.OK;
        boolean needsUpdate = false;
        *//*if (!Objects.equals(changedUser.getFirebaseUID(), user.getFirebaseUID())) {
            user.setFirebaseUID(changedUser.getFirebaseUID());
            needsUpdate = true;
        }*//*
        if (needsUpdate) {
            playerRepository.save(player);
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(player, httpStatus);
    }


    @DeleteMapping("{id}")
    public ResponseEntity<Player> deletePlayer(@PathVariable Long id) {
        if (playerRepository.existsById(id)) {
            playerRepository.deleteById(id);
            httpStatus = HttpStatus.OK;
        }
        else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }*/
}
