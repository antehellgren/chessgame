package se.experis.backend.controllers;


import se.experis.backend.models.Message;
import se.experis.backend.repositories.FriendsRepository;
import se.experis.backend.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api/v1/message")
public class MessageController {
    @Autowired
    private FriendsRepository friendsRepository;
    HttpStatus httpStatus;

    @Autowired
    private MessageRepository messageRepository;


    @GetMapping("list")
    public Iterable<Message> getMessage() {
        return this.messageRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Message> getMessageById(@PathVariable Long id) {
        Message returnMessage = new Message();
        HttpStatus status;

        if (messageRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnMessage = messageRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMessage, status);
    }

    @PostMapping
    public ResponseEntity<Message> addMessage(@RequestBody Message message) {
        httpStatus = HttpStatus.FORBIDDEN;
        try {
            message = messageRepository.save(message);
            httpStatus = HttpStatus.CREATED;
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(message, httpStatus);
    }


    @PatchMapping("{id}")
    public ResponseEntity<Message> updateMessage(@PathVariable Long id, @RequestBody Message changedMessage) {
        Message message = messageRepository.findById(id).get();
        httpStatus = HttpStatus.OK;
        boolean needsUpdate = false;
        if (needsUpdate) {
            messageRepository.save(message);
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(message, httpStatus);
    }


   /* @DeleteMapping("{id}")
    public ResponseEntity<Message> deleteMessage(@PathVariable Long id) {
        if (messageRepository.existsById(id)) {
            messageRepository.deleteById(id);
            httpStatus = HttpStatus.OK;
        }
        else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }*/
}

