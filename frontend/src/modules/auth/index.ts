import { 
	getAuth,
	signInWithRedirect,
	GoogleAuthProvider,
	/* getRedirectResult,
	UserCredential */ } from "firebase/auth";
	

	const auth = getAuth();
	const provider = new GoogleAuthProvider();
  
/* export const getGoogleLoginResults = ():void => {
	getRedirectResult(auth)
		.then((result: UserCredential | null) => {
			// This gives you a Google Access Token. You can use it to access Google APIs.
			const credential = GoogleAuthProvider.credentialFromResult(result!);
			const beerToken = credential!.accessToken; // the one and only
			console.log(credential)

			// The signed-in user info.
			const user = result!.user;
		}).catch((error) => {
			// Handle Errors here.
			const errorCode = error.code;
			const errorMessage = error.message;
			// The email of the user's account used.
			const email = error.email;
			// The AuthCredential type that was used.
			const credential = GoogleAuthProvider.credentialFromError(error);
			// ...
		});
}; */

export const loginWithGoogle = ():void => {
	signInWithRedirect(auth, provider);
};
