// src/features/auth/authSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk } from '../../store';
import { RootState } from '../../rootReducer';
import { createNewPlayer, getPlayerById } from '../../../services/authService';
import { auth } from '../../../firebase';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from 'firebase/auth';

export interface AuthError {
  message: string
};

export interface AuthState {
  isAuth: boolean
  currentUser?: CurrentUser
  isLoading: boolean
  error: AuthError
};

export interface CurrentUser {
  uid: string
  username: string
  email: string
  accessToken: string
};
export const initialState: AuthState = {
  currentUser: undefined,
  isAuth: false,
  isLoading: false,
  error: {
    message: 'An error occured'
  },
};

export const register = (data: any): AppThunk => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const firebaseResponse = await createUserWithEmailAndPassword(auth, data.email, data.password);
    const { accessToken, email, displayName, uid }:any = firebaseResponse.user;
    console.log(accessToken);
    
    const userData = {
      username: displayName ? displayName : data.username,
      email,
      uid,
      accessToken
    };
    
    const response = await createNewPlayer(userData);
    
    dispatch(setAuthSuccess(response.data));
  } catch (error) {
    dispatch(setAuthFailed(error));
  } finally {
    dispatch(setLoading(false));
  };
};

export const login = (data:any): AppThunk => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const firebaseResponse = await signInWithEmailAndPassword(auth, data.email, data.password)
    const { accessToken, uid }:any = await firebaseResponse.user;
    console.log(accessToken);
    
    
    const response = await getPlayerById(uid, accessToken);

    const userData = {
      ...response.data,
      accessToken: accessToken
    };
    
    dispatch(setAuthSuccess(userData));
  } catch (error) {
    dispatch(setAuthFailed(error));
  } finally {
    dispatch(setLoading(false));
  };
};

export const syncAuthState = (data:any): AppThunk => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const { uid, accessToken } = data;
    const response = await getPlayerById(uid, accessToken);

    const userData = {
      ...response.data,
      accessToken: accessToken
    };
    
    dispatch(setAuthSuccess(userData));
  } catch (error) {
    dispatch(setAuthFailed(error));
  } finally {
    dispatch(setLoading(false));
  };
};

export const logOut = (): AppThunk => async (dispatch) =>  {
  try {
    dispatch(setLoading(true));
    await signOut(auth);
    indexedDB.deleteDatabase('firebaseLocalStorage');
    dispatch(setLogOut());
  } catch (error) {
    dispatch(setAuthFailed(error));
  } finally {
    dispatch(setLoading(false));
  };
};
export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoading: (state, {payload}: PayloadAction<boolean>) => {
      state.isLoading = payload;
    },
    setAuthSuccess: (state, { payload }: PayloadAction<any>) => {
      state.currentUser = payload;
      state.isAuth = true;
    },
    setLogOut: (state) => {
      state.isAuth = false;
      state.currentUser = undefined;
    },
    setAuthFailed: (state, { payload }: PayloadAction<any>) => {
      state.error = payload;
      state.isAuth = false;
    },
  },
});

export const { setAuthSuccess, setLogOut, setLoading, setAuthFailed} = authSlice.actions;



export const authSelector = (state: RootState) => state.authReducer;
export default authSlice.reducer;