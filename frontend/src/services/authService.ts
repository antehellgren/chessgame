import { BASE_API_URL } from '../constants';

const axios = require('axios').default;

const BASE_URL:string = `${BASE_API_URL}/api/v1/players`;

const config = {
  headers: {
    'Content-Type': 'application/json',
  }
};


export const createNewPlayer = ({ uid, username, email, accessToken }:any):Promise<any> => {
  Object.assign(config.headers, { Authorization: 'Bearer ' + accessToken });
  return axios.post(BASE_URL, { uid, username , email }, config);
};


export const getPlayerById = (id: string, accessToken: string | undefined):Promise<any> => {
  Object.assign(config.headers, { Authorization: 'Bearer ' + accessToken });
  return axios.get(`${BASE_URL}/` + id, config);
};
