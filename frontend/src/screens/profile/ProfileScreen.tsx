import IScreenProps from '../../interface/screen.interface';

const ProfileScreen: React.FC<IScreenProps> = (props) => {

	return (
		<div className="profile-container">
			<div className="profile-details">
				<p>NAME: {}</p>
				<p>EMAIL: {}</p>
			</div>
		</div>
	);
};

export default ProfileScreen;
