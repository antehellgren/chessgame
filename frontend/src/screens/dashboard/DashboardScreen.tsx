import React from 'react';
import { useHistory } from 'react-router-dom';
import IScreenProps from '../../interface/screen.interface';
import './DashboardScreen.css';

const DashboardScreen: React.FC<IScreenProps> = (props) => {

  const history = useHistory();

  const onClickPlay = (): void => {
    console.log('play');
    history.push('/game')

  }
  const onClickProfile = (): void => {
    console.log('profile');
    history.push("/profile")
  }
  const onClickChat = (): void => {
    console.log('chat');
  }

  return (
    <div className="dashboard-container">
      <div className="dashboard-menu-wrapper">
        <button
          onClick={onClickPlay}
          className="dashboard-btn play-btn"
          type="button"
        >
          Play game
        </button>
        <button
          onClick={onClickProfile}
          className="dashboard-btn profile-btn"
          type="button"
        >
          Profile
        </button>
        <button
          onClick={onClickChat}
          className="dashboard-btn chat-btn"
          type="button"
        >
          Messages
        </button>
      </div>
    </div>
  );
};

export default DashboardScreen;
