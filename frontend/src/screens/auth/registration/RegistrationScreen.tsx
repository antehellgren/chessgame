import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import IScreenProps from "../../../interface/screen.interface";
import { register } from '../../../redux/feature/auth/authSlice';
import { Button, Input } from '@mui/material';
import LoginIcon from '@mui/icons-material/Login';
import './RegistrationScreen.css';

const RegistrationScreen: React.FC<IScreenProps> = (props) => {

	const [userDetails, setUserDetails] = useState({
		email: '',
		username: '',
		password: ''
	});
  const [confirmPassword, setConfirmPassword] = useState('');

	const dispatch = useDispatch();
  const history = useHistory();

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
		setUserDetails({ ...userDetails, [e.currentTarget.name]: e.currentTarget.value })
	};
	const handleClick = (): void => {
    if(confirmPassword === userDetails.password) 
		  dispatch(register(userDetails));
	};
  const handleConfirmPassword = (e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>):void => {
    setConfirmPassword(e.target.value);
  };
  const handleClickLogin = (): void => {
		history.push("/auth/login");
	};
	return (
		<div className="registration-container">
			<div className="registration-menu-container">
				<h1>REGISTER</h1>
				<div className="registration-menu-wrapper">
					<Input 
            name="username"
            type="username"
            id="username"
            placeholder="Username"
            onChange={(e) => handleChange(e)} />
					<Input 
            name="email" 
            type="email" 
            id="email" 
            placeholder="Email"
            onChange={(e) => handleChange(e)} />
					<Input 
            name="password" 
            type="password" 
            id="password" 
            placeholder="Password"
            onChange={(e) => handleChange(e)} />
					<Input 
            name="password" 
            type="password" 
            id="password" 
            placeholder="Confirm password"
            onChange={(e) => handleConfirmPassword(e)} />
          <a>
            Already have an account? 
            <Button 
              onClick={handleClickLogin} 
              endIcon={<LoginIcon />} 
              size='small'
              >Log in </Button>
          </a>
					<Button 
            onClick={handleClick} 
            variant='contained' 
            >Register</Button>
				</div>
			</div>
		</div>
	);
};

export default RegistrationScreen;
