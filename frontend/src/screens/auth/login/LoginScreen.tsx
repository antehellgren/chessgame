import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getAuth, onAuthStateChanged } from '@firebase/auth';
import { loginWithGoogle } from '../../../modules/auth';
import IScreenProps from '../../../interface/screen.interface';
import { login } from '../../../redux/feature/auth/authSlice';
import { Button, Input } from '@mui/material';
import GoogleIcon from '@mui/icons-material/Google';
import LoginIcon from '@mui/icons-material/Login';
import HowToRegOutlinedIcon from '@mui/icons-material/HowToRegOutlined';
import './LoginScreen.css';

const LoginScreen: React.FC<IScreenProps> = (props) => {

	const [userDetails, setUserDetails] = useState({
		email: '',
		password: ''
	});
	const auth = getAuth();
	const history = useHistory();
	const dispatch = useDispatch();

	useEffect(() => {
		onAuthStateChanged(auth, (user: any) => {
			if (user) {
				history.push('/');
			};
		});
	}, []);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
		setUserDetails({ ...userDetails, [e.currentTarget.name]: e.currentTarget.value });
	};

	const handleClick = (): void => {
		dispatch(login(userDetails));
		history.push('/');
	};
  
	const handleGoogleLogin = (): void => {
		loginWithGoogle();
	};

	const handleClickRegister = (): void => {
		history.push("/auth/register");
	};

	return (
		<div className="login-container">
			<div className="login-menu-container">
				<h1>LOG IN</h1>
				<div className="login-menu-wrapper">
					<Input 
            name="email" 
            type="email" 
            id="email" 
            placeholder="Email"
            onChange={(e) => handleChange(e)} />
					<Input 
            name="password" 
            type="password" 
            id="password" 
            placeholder="Password"
            onChange={(e) => handleChange(e)} />
				</div>
        <div className='login-menu-buttons'>
          <Button 
            onClick={handleClick} 
            endIcon={<LoginIcon />}
            variant='contained'
            >Log in </Button>
          <Button 
            onClick={handleClickRegister} 
            endIcon={<HowToRegOutlinedIcon />}
            variant='contained'
            >Register</Button>
        </div>
        <a>OR</a>
        <div className='login-menu-footer'>
          <Button 
            onClick={handleGoogleLogin} 
            endIcon={<GoogleIcon />}
            >Sign in with Google </Button>
        </div>
			</div>
		</div>
	);
};

export default LoginScreen;


