import React, { useEffect, useState } from 'react';
import Chessground from '@react-chess/chessground';
import IScreenProps from '../../interface/screen.interface';
import { Chess } from 'chess.ts';
import { database } from '../../firebase';
import { set, ref, onValue, push, child, get, off } from 'firebase/database';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/rootReducer';
import { getPlayerById } from '../../services/authService';
import { Button } from '@mui/material';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import './GameScreen.css';


const GameScreen: React.FC<IScreenProps> = ():JSX.Element => {

	const [chess] = useState<Chess>(
    new Chess()
  );

  const [playerNum, setPlayerNum] = useState(0);
  const [gameState, setGameState] = useState({
    p1_token: '',
    p2_token: '',
    fen: chess.fen(),
    moves: '',
    turn: chess.turn()
  });
	const [gameId, setGameId] = useState('');
	const [showTooltip, setShowTooltip] = useState(false);
  const [showGameInfo, setShowGameInfo] = useState(true);
	const [userInput, setUserInput] = useState('');
  const [opponent, setOpponent] = useState({
    uid: '',
    username: '',
    email: '',
    game: [],
    playerStats: '',
    chat: [],
    friends: []
  });
  const currentUser = useSelector((state: RootState) => state.authReducer.currentUser);
  
  useEffect(() => {
    listenForUpdates();
  }, [gameId]);
  
  useEffect(() => {
    const playerNum = figurePlayer(currentUser?.uid, gameState)
    setPlayerNum(playerNum);    // Decide which player is the current user
  }, [gameState, currentUser]);
  
  const listenForUpdates = ():void => {
    off(ref(database, 'games/'));                               // Remove any existing listeners
    if(gameId) {
      const gameRef = ref(database, 'games/' + gameId);
      onValue(gameRef, (snapshot) => {                          // Add listener to specified game(id) and sync it with the chess board.
        const data = snapshot.val();
        chess.load(data.fen);
        setGameState({...data});
      });
    }
  }

  const figurePlayer = (uid:any, { p1_token, p2_token }:any) => {
    if (uid === p1_token) {
      if(gameState.p2_token) {
        getPlayerById(gameState.p2_token, currentUser?.accessToken)
        .then(res => setOpponent(res.data))  
      }
      return 1;
    } else if (uid === p2_token) {
      if(gameState.p1_token) {
        getPlayerById(gameState.p1_token, currentUser?.accessToken)
        .then(res => setOpponent(res.data))
      }
      return 2;
    } else {
      return 0;
    }
  }
  const isMyTurn = (turn:string):boolean => {
    return (playerNum === 1 && turn === 'w') || (playerNum === 2 && turn === 'b');
  }

  /**
   * 
   * @param moves String containing history of all moves made in a game, e.g "e2-e4,b8-c6,f1-b5".
   * @param move String containing a single move to be added to game history, e.g "e2-e4".
   * @returns String of move history including the provided move.
   */
  const pushMove = (moves:string, move:string):string => {
    if (!moves) {
      return [move].join(",");
    } else {
      const arr = moves.split(",");
      return [...arr, move].join(",");
    }
  }

  /**
   * @desc Checks if the provided move is allowed and updates the state with the new move. 
   *  If the move was not legal, the state is updated with the previous state.
   * @param move Object containing source square and target square of a move, along with optional promotion preference.
   *   e.g { from: 'e2', to: 'e4', promotion: 'q' }
   */
  const handleMove = (move:any):void => {
    if(isMyTurn(gameState.turn)) {
      const m = chess.move(move);
       if (m) { 
        const updateState:any = {
          p1_token: gameState.p1_token || '',
          p2_token: gameState.p2_token || '',
          fen: chess.fen(),
          moves: pushMove(gameState.moves, `${m['from']}-${m['to']}`),
          turn: chess.turn()
        }
        set(ref(database, `/games/${gameId}` ), updateState);
      }
      else {
        setGameState({...gameState, fen: chess.fen()});
      }
    }
    else {
      setGameState({...gameState, fen: chess.fen()});
    }
  }

	const statusText = (turn: string, in_mate: any, in_draw: any, in_check: any):string => {
		const moveColor = turn === 'b' ? "Black" : "White";
		if (in_mate)
			return `Game Over, ${moveColor} is in checkmate`;
		else if (in_draw)
			return 'Game over, drawn position';
		else if (in_check)
			return `${moveColor} is in check!`;
		else
			return "";
	}

  /**
   * @desc Creates a new empty game object in the RTDB with a generated ID, 
   * saves a reference to it and pushes initial game state with the ID of hosting player into both the DB and local state.
   */ 
	const createGame = async():Promise<void> => {
		const game = await push(child(ref(database), 'games'));
		const newGame = {
			p1_token: currentUser ? currentUser.uid : '',
			p2_token: '',
			fen: chess.fen(),
			moves: '',
			turn: chess.turn(),
		};
		set(ref(database, 'games/' + game.key), newGame);
    setGameState(newGame);
    setGameId(game?.key!);
	}

	const handleChange = (e:any):void => {
    setUserInput(e.target.value);
	}

  /**
   * @desc Attempts to retrieve a game by querying the ID specified by the user.
   * Then adds the user ID of the current player to the game object before pushing it back to the DB.
   * 
   */
	const handleJoinGame = ():void => {
		get(child(ref(database), `games/${userInput}`)).then((snapshot) => {
			if (snapshot.exists()) {
				const data = snapshot.val();
				data.p2_token = currentUser?.uid;
        set(ref(database, `games/${userInput}` ), data);
        setGameState(data);
        setGameId(userInput);
			} else {
				console.log("Game not found - No data available");
			}
		}).catch((error) => {
			console.error(error);
		});
	}

  /**
   * @desc Handles the copy button where the user can copy their game ID with a simple click.
   */
  const onClickCopy = async () => {
    try {
      await navigator.clipboard.writeText(gameId);
      setShowTooltip(true);
      setTimeout(() => {
        setShowTooltip(false);
      }, 3000);
      console.log('works');
    }
    catch(err) {
      console.error('error: ' + err);
    }
  }
  const toggleGameInfo = ():void => {
    setShowGameInfo(!showGameInfo);
  }

	const config:any = {
		fen: gameState.fen,   // Board position
    orientation: playerNum === 1 ? 'white' : 'black' ,
    turnColor: 'white', 
		events: {
			// 'move' function runs every time a piece is moved on the board.
			move: (orig:any, dest:any) => {
				handleMove({
					from: orig,
					to: dest,
					promotion: 'q'
				});
			}
		}
	}; 

	//måste förstå config.  EZ HEJJA! https://github.com/ornicar/chessground/blob/master/src/config.ts#L7-L90

	return (
		<div>
      {
        gameId 
        ?
          <div>
            <div className='gamescreen-game-wrapper'>
              <div className='gamescreen-game-opponent'>
                <div>
                  {
                    opponent.username && 
                    <span>
                      <h3>{opponent.username}</h3>
                      <p>1337 ELO</p>
                    </span>
                  }
                </div>
              </div>
              <Chessground width={700} height={700} config={config} />
              <div className='gamescreen-game-currentplayer'>
                <div>
                  <h3>{currentUser?.username}</h3>
                </div>
                <p>0 ELO</p>
              </div>
            </div>
            <p>{statusText(chess.turn(), chess.inCheckmate(), chess.inDraw(), chess.inCheck())}</p>
            {
              showGameInfo
              ?
              <React.Fragment>
                <div className='gamescreen-info'>
                  <h3>Your game ID: </h3>
                  <div className='gamescreen-info-gameid'>
                    <span>
                      <p>{gameId}</p>
                      <Button 
                        disableRipple
                        startIcon={<ContentCopyIcon />}
                        onClick={onClickCopy}
                        />
                    </span>
                  </div>
                  {
                    showTooltip 
                    ? 
                      <div id="tooltip" className="top">
                        <div className="tooltip-arrow" />
                        <div className="tooltip-label">Copied <CheckCircleIcon fontSize='small'/></div>
                      </div>  
                    : null
                  }
                </div>
                <span><p>Tip: Send this code to a friend and they will be able to connect to your game.</p></span>
                <Button variant='contained' color='secondary' onClick={toggleGameInfo}>Hide game ID</Button>
              </React.Fragment>
              :
              <Button variant='contained' color='info' onClick={toggleGameInfo}>Game info</Button>
            } 
          </div>
        :
          <div className='gamescreen-controls'>
            <Button
              onClick={createGame}
              variant='contained'
              >Create new game</Button>
            
            <Button
              onClick={handleJoinGame}
              variant='contained'
              >Join Game</Button>
              
            <span>
              <label htmlFor="gameid" >Join with Game ID: </label>
              <input 
                id="gameid"
                placeholder='Example -Mn6YNVe23b0OAbN1hD6'
                onChange={handleChange}
                />
            </span>
          </div>
      }
		</div>
	);
};

export default GameScreen;
