import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, RouteComponentProps } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { onAuthStateChanged } from 'firebase/auth';
import NavbarComponent from './components/navbar/NavbarComponent';
import SidebarComponent from './components/sidebar/SidebarComponent';
import routes from './config/routes';
import AuthRoute from './modules/auth/AuthRoute';
import { syncAuthState } from './redux/feature/auth/authSlice';
import { auth } from './firebase';
import './App.css';

export interface IApplicationProps { }

const App: React.FunctionComponent<IApplicationProps> = () => {

  const dispatch = useDispatch();

  useEffect(() => {
    onAuthStateChanged(auth, user => { 
    console.log(user); 
    if(user !== null) {
      const { uid, accessToken }:any = user;
      const data:any = {
        uid,
        accessToken
      };
      dispatch(syncAuthState(data));
    };
  });
  }, []);
  
  return (  
    <Router>
      <NavbarComponent name="navbar" />
      <SidebarComponent name="sidebar" />
      <div className="content">
        <Switch>
          {routes.map((route, index) =>
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              render={(routeProps: RouteComponentProps<any>) => {
                if (route.protected)
                  return <AuthRoute ><route.component {...routeProps} /></AuthRoute>;

                return <route.component  {...routeProps} />;
              }}
            />)}
        </Switch>
      </div>
    </Router>
  );
}

export default App;