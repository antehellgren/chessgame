import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import IScreenProps from '../../interface/screen.interface';
import { logOut } from '../../redux/feature/auth/authSlice';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import { RootState } from '../../redux/rootReducer';
import { Button } from '@mui/material';
import './NavbarComponent.css';

const NavbarComponent: React.FC<IScreenProps> = () => {

  const history = useHistory();
  const dispatch = useDispatch();
  const currentUser = useSelector((state: RootState) => state.authReducer.currentUser);

  const onClickLogout = (): void => {
    dispatch(logOut());
    history.push('/auth/login');
  };

  const onClickLogin = (): void => {
    history.push('/auth/login');
  };

  const onClickHome = (): void => {
    history.push('/');
  };
  return (
    <div className="navbar-container">
      {
        currentUser ?
        <div className="navbar-user-active">
          <Button 
            onClick={onClickLogout} 
            endIcon={<LogoutOutlinedIcon/>}
            variant='text'
            size='small'
            >
            Log out
          </Button>
          <div className="navbar-user-details">
            <p>{ currentUser.email }</p>
            <div>
              <AccountCircleOutlinedIcon fontSize='large' />
            </div>
          </div>
         
          
        </div>  :
        <div className="navbar-buttons">
        <Button onClick={onClickHome} >
          Home
        </Button>
        <Button onClick={onClickLogin}>
          Log in
        </Button>
        </div>
      }
    </div>
  );
};

export default NavbarComponent;
