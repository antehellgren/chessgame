import React from 'react';
import { NavLink } from 'react-router-dom';
import IScreenProps from '../../interface/screen.interface';
import logo from '../../media/images/logo.png';
import './SidebarComponent.css';

const SidebarComponent: React.FC<IScreenProps> = ():JSX.Element => {
	return (
		<div className="sidebar-container">
			<div className="sidebar-logo">
				<img alt="logo" src={logo} width={200} height={200} />
			</div>
			<h1>Knug.com</h1>
      <div>
        <NavLink to="#"><p>Friends</p></NavLink>
      </div>
      
      
		</div>
	);
};

export default SidebarComponent;
