import IRoute from "../interface/route.interface";
import RegistrationScreen from "../screens/auth/registration/RegistrationScreen";
import LoginScreen from "../screens/auth/login/LoginScreen";
import DashboardScreen from "../screens/dashboard/DashboardScreen";
import GameScreen from "../screens/game/GameScreen";
import ProfileScreen from "../screens/profile/ProfileScreen";

const routes: IRoute[] = [
  {
    path: '/',
    exact: true,
    component: DashboardScreen,
    name: 'Dashboard',
    protected: true
  },
  {
    path: '/game',
    exact: true,
    component: GameScreen,
    name: 'Game',
    protected: true
  },
  {
    path: '/profile',
    exact: true,
    component: ProfileScreen,
    name: 'Profile',
    protected: true
  },
  {
    path: '/auth/login',
    exact: true,
    component: LoginScreen,
    name: 'Login',
    protected: false
  },
  {
    path: '/auth/register',
    exact: true,
    component: RegistrationScreen,
    name: 'Registration',
    protected: false
  },
];
export default routes;